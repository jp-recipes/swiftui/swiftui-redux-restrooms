//
//  swiftui_redux_restroomsApp.swift
//  swiftui-redux-restrooms
//
//  Created by JP on 24-08-23.
//

import SwiftUI

@main
struct RestroomsReduxApp: App {

    init() {
        configureTheme()
    }

    var body: some Scene {
        let store = Store(reducer: appReducer, state: AppState(), middlewares: [
            restroomsMiddleware()
        ])

        WindowGroup {
            HomeScreen().environmentObject(store)
        }
    }

    private func configureTheme() {
        UINavigationBar.appearance().backgroundColor = UIColor(displayP3Red: 44/255, green: 62/255, blue: 80/255, alpha: 1.0)
    }
}
