//
//  HomeScreen.swift
//  RestRoomFinder
//
//  Created by JP on 24-08-23.
//

import SwiftUI
import Combine

struct HomeScreen: View {
    @StateObject private var locationManager = LocationManager()
    @EnvironmentObject var store: Store<AppState>
    @State private var cancellables: AnyCancellable?

    private struct Props {
        let restrooms: [Restroom]
        let onFetchRestroomsByLatLng: (Double, Double) -> Void
    }

    private func map(state: RestroomState) -> Props {
        Props(restrooms: state.restrooms) { lat, lng in
            store.dispatch(action: FetchRestroomsAction(latitude: lat, longitude: lng))
        }
    }

    var body: some View {
        let props = map(state: store.state.restrooms)

        ZStack(alignment: .leading) {
            Color(#colorLiteral(red: 0.880972445, green: 0.3729454875, blue: 0.2552506924, alpha: 1))

            VStack {
                HStack {
                    EmptyView()
                }.frame(maxHeight: 44)

                Spacer()

                HStack {
                    Text("Restrooms")
                        .foregroundColor(.white)
                        .font(.largeTitle)

                    Spacer()

                    Button {
                        locationManager.updateLocation()
                    } label: {
                        Image(systemName: "arrow.clockwise.circle")
                            .font(.title)
                            .foregroundColor(.white)
                    }
                }
                .padding(.horizontal)

                List(props.restrooms) { restroom in
                    RestroomCell(restroom: restroom)
                }
                .listStyle(.plain)
            }
        }
        .ignoresSafeArea()
        .onAppear {
            cancellables = locationManager.$location
                .sink { location in
                    if let location = location {
                        props.onFetchRestroomsByLatLng(
                            location.coordinate.latitude,
                            location.coordinate.longitude
                        )
                    }
                }
        }
    }
}

struct HomeScreen_Previews: PreviewProvider {
    static var previews: some View {
        let store = Store(reducer: appReducer, state: AppState(), middlewares: [
            restroomsMiddleware()
        ])
        return HomeScreen().environmentObject(store)
    }
}

struct RestroomCell: View {
    let restroom: Restroom

    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            HStack {
                Text(restroom.name ?? "Not Available")
                    .font(.headline)
                Spacer()
                Text("\(restroom.distance, specifier: "%.2f") miles")
            }
            .padding(.top, 10)

            Text(restroom.address)
                .font(.subheadline)
                .opacity(0.5)

            Button("Directions") {
                guard let url = URL(string: "https://maps.apple.com/?address=\(restroom.address.encodeURL() ?? "")") else {
                    return
                }

                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url)
                }
            }
            .font(.caption)
            .foregroundColor(.white)
            .padding(6)
            .background(Color(#colorLiteral(red: 0.184266597, green: 0.8003296256, blue: 0.4435204864, alpha: 1)))
            .cornerRadius(6)

            Text(restroom.comment ?? "")
                .font(.footnote)

            HStack {
                Text(restroom.accessible ? "♿️" : "")
            }
        }
    }
}
