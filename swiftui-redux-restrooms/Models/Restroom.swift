//
//  Restroom.swift
//  swiftui-redux-restrooms
//
//  Created by JP on 24-08-23.
//

import Foundation

struct Restroom: Decodable, Identifiable {
    let id: Int
    let name: String?
    let street: String
    let city: String
    let state: String
    let accessible: Bool
    let unisex: Bool
    let distance: Double
    let comment: String?
    let latitude: Double
    let longitude: Double

    var address: String {
        "\(street), \(city) \(state)"
    }
}
