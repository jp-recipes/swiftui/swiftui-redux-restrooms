//
//  RestroomsMiddleware.swift
//  swiftui-redux-restrooms
//
//  Created by JP on 24-08-23.
//

import Foundation

func restroomsMiddleware() -> Middleware<AppState> {
    return { state, action, dispatch in
        switch action {
        case let action as FetchRestroomsAction:
            getRestroomsByLatAndLng(action: action, dispatch: dispatch)
        default:
            break
        }
    }
}

private func getRestroomsByLatAndLng(action: FetchRestroomsAction, dispatch: @escaping Dispatcher) {
    Webservice().getRestroomsByLatAndLng(lat: action.latitude, lng: action.longitude) { result in
        switch result {
        case .success(let restrooms):
            dispatch(SetRestroomsAction(restrooms: restrooms))
        case .failure(let error):
            print(error.localizedDescription)
        }
    }
}
