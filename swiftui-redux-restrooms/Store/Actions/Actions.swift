//
//  Actions.swift
//  RestRoomFinder
//
//  Created by JP on 24-08-23.
//

import Foundation

protocol Action { }

struct FetchRestroomsAction: Action {
    let latitude: Double
    let longitude: Double
}

struct SetRestroomsAction: Action {
    let restrooms: [Restroom]
}
