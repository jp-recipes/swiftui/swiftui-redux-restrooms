//
//  RestroomsReducer.swift
//  swiftui-redux-restrooms
//
//  Created by JP on 24-08-23.
//

import Foundation

func restroomsReducer(_ state: RestroomState, _ action: Action) -> RestroomState {
    var state = state

    switch action {
    case let action as SetRestroomsAction:
        state.restrooms = action.restrooms
    default:
        break
    }

    return state
}
