//
//  String+Extentions.swift
//  RestRoomFinder
//
//  Created by JP on 24-08-23.
//

import Foundation

extension String {
    
    func encodeURL() -> String? {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
    }
    
}
